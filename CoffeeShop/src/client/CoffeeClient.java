package client;

import java.util.ArrayList;

import controller.CartController;
import controller.LoginController;
import controller.MenuController;
import controller.RegisterController;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

public class CoffeeClient extends Application {
	
	private Scene loginScene;
	private Scene registerScene;
	private Scene menuScene;
	private Scene cartScene;
	private LoginController loginEvent;
	private RegisterController registerEvent;
	private MenuController menuEvent;
	private CartController cartEvent;
	private String selectedItem; 
	
	public static final ObservableList<String> itemsList = FXCollections.observableArrayList();
	
	public static void main(String[] args) {	

	    launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
	    	
    	//-=-=-=-=-=-=-=-=-LOGIN-=-=-=-=-=-=-=-=-
        GridPane gridLogin = new GridPane();
        gridLogin.setAlignment(Pos.CENTER);
        gridLogin.setHgap(5);
        gridLogin.setVgap(5);
        gridLogin.setPadding(new Insets(25, 25, 25, 25));

        Text loginTitle = new Text("Welcome to Coffee Zone");
        loginTitle.setFont(Font.font("Verdana", FontWeight.NORMAL, 20));
        gridLogin.add(loginTitle, 0, 0, 2, 1);

        Label username = new Label("Username:");
        gridLogin.add(username, 0, 1);

        TextField userTextField = new TextField();
        userTextField.setPromptText("Enter your username");
        gridLogin.add(userTextField, 1, 1);

        Label password = new Label("Password:");
        gridLogin.add(password, 0, 2);

        PasswordField passwordBox = new PasswordField();
        passwordBox.setPromptText("Enter your password");
        gridLogin.add(passwordBox, 1, 2);

        VBox loginButtons = new VBox();
        loginButtons.setSpacing(5);
	        
        Button menuButton = new Button("View menu");
        menuButton.setOnAction(e -> stage.setScene(menuScene));
        Button signIn = new Button("Sign in");	
        
        loginEvent = new LoginController("login", userTextField, passwordBox);
        signIn.setOnAction(loginEvent);

        HBox signInBox = new HBox();
        signInBox.setAlignment(Pos.CENTER_RIGHT);
        signInBox.setSpacing(10);
        signInBox.getChildren().addAll(menuButton, signIn);
        
        
        Button newAccount = new Button("Create new account");           
        newAccount.setOnAction(e -> stage.setScene(registerScene));

        HBox newAccountBox = new HBox();
        newAccountBox.setAlignment(Pos.BOTTOM_RIGHT);
        newAccountBox.getChildren().addAll(newAccount);
	        
        loginButtons.getChildren().addAll(signInBox, newAccountBox);
        gridLogin.add(loginButtons, 1, 4);
	        
	        
/*
	        final Text actiontarget = new Text();
	        grid.add(actiontarget, 1, 6);

	        btn.setOnAction(new EventHandler<ActionEvent>() {

	            @Override
	            public void handle(ActionEvent e) {
	                actiontarget.setFill(Color.FIREBRICK);
	                actiontarget.setText("Sign in button pressed");
	            }
	        });
*/        
	    //-=-=-=-=-=-=-=-=-REGISTER-=-=-=-=-=-=-=-=-
        GridPane gridRegister = new GridPane();
		gridRegister.setAlignment(Pos.CENTER);
		gridRegister.setHgap(5);
		gridRegister.setVgap(5);
		gridRegister.setPadding(new Insets(25, 25, 25, 25));

        Text registerTitle = new Text("Your info");
        registerTitle.setFont(Font.font("Verdana", FontWeight.NORMAL, 20));
        gridRegister.add(registerTitle, 0, 0, 2, 1);
        
        //Username
        Label newUser = new Label("Username:");
        gridRegister.add(newUser, 0, 1);
        TextField newUserTextField = new TextField();
        newUserTextField.setPromptText("Enter your username");
        gridRegister.add(newUserTextField, 1, 1);
        
        //Password
        Label newPass = new Label("Password:");
        gridRegister.add(newPass, 0, 2);
        PasswordField newPassField = new PasswordField();
        newPassField.setPromptText("Enter your password");
        gridRegister.add(newPassField, 1, 2);
        
        //Address
        Label address = new Label("Address:");
        gridRegister.add(address, 0, 3);
        TextField addressField = new TextField();
        addressField.setPromptText("Enter your address");
        gridRegister.add(addressField, 1, 3);
        
        //City
        Label city = new Label("City:");
        gridRegister.add(city, 0, 4);
        TextField cityField = new TextField();
        cityField.setPromptText("Enter your city");
        gridRegister.add(cityField, 1, 4);
        
        //Zip
        Label zip = new Label("ZIP code:");
        gridRegister.add(zip, 0, 5);
        TextField zipField = new TextField();
        zipField.setPromptText("Enter your zip");
        gridRegister.add(zipField, 1, 5);
        
        //Phone
        Label phone = new Label("Phone# (Optional):");
        gridRegister.add(phone, 0, 6);
        TextField phoneField = new TextField();
        phoneField.setPromptText("Enter your zip");
        gridRegister.add(phoneField, 1, 6);
        
        //Email
        Label email = new Label("Email (Optional):");
        gridRegister.add(email, 0, 7);
        TextField emailField = new TextField();
        emailField.setPromptText("Enter your email");
        gridRegister.add(emailField, 1, 7);

        VBox registerButtons = new VBox();
        registerButtons.setSpacing(5);
	        
        Button createAccountButton = new Button("Create account");
        registerEvent = new RegisterController("register", newUserTextField, newPassField, addressField, cityField, zipField, phoneField, emailField);
        createAccountButton.setOnAction(registerEvent);
        HBox createAccountBox = new HBox(10);
        createAccountBox.setAlignment(Pos.BOTTOM_RIGHT);
        createAccountBox.getChildren().add(createAccountButton);
	        
        Button backButton = new Button("Already have an account?");
        backButton.setOnAction(e -> stage.setScene(loginScene));
        HBox backBox = new HBox(10);
        backBox.setAlignment(Pos.BOTTOM_LEFT);
        backBox.getChildren().add(backButton);
	        
        registerButtons.getChildren().addAll(createAccountBox, backBox);
        gridRegister.add(registerButtons, 1, 9);
	        
        //-=-=-=-=-=-=-=-=-MENU-=-=-=-=-=-=-=-=-
    		GridPane gridMenu = new GridPane();
    		gridMenu.setAlignment(Pos.CENTER);
    		gridMenu.setHgap(50);
    		gridMenu.setVgap(5);
    		gridMenu.setPadding(new Insets(25, 25, 25, 25));
    				
    		VBox welcomeBox = new VBox();
            Text menuTitle = new Text("Menu");
            menuTitle.setFont(Font.font("Verdana", FontWeight.NORMAL, 20));
            welcomeBox.getChildren().add(menuTitle);
            welcomeBox.setAlignment(Pos.CENTER);
            gridMenu.add(welcomeBox, 2, 0);
            
            Label itemsLabel = new Label("Store Availability");
            itemsLabel.setFont(Font.font("Calibri", FontWeight.BOLD, 20));
            gridMenu.add(itemsLabel, 1, 1);
            Label cartLabel = new Label("Your cart");
            cartLabel.setFont(Font.font("Calibri", FontWeight.BOLD, 20));
            gridMenu.add(cartLabel, 3, 1);
            
            //Menu
            ListView<String> itemsView = new ListView<String>();
            itemsList.addAll(
            		"americano-3.00$", "cappucino-3.50$", "latte-3.00$", "expresso-2.00$",
            		"croissant-2.00$", "english muffin-4.00$", "bagel and cream-2.50$", "cinnamon roll-3.00$",
            		"sugar-0.15$", "milk-0.15$");
            itemsView.getItems().addAll(itemsList);
            itemsView.setPrefSize(30, 250);
            itemsView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
            gridMenu.add(itemsView, 1, 2);
            
            Button createOrder = new Button("Create new order");
            gridMenu.add(createOrder, 1, 3);
            
            //Quantity
            TextField quantity = new TextField();
            quantity.setPromptText("Enter your quantity");
            gridMenu.add(quantity, 2, 2);
            
            //Current cart
            TextArea cartView = new TextArea();
            cartView.setPrefSize(30, 250);
            gridMenu.add(cartView, 3, 2);
            Button checkout = new Button("Proceed to checkout");
            gridMenu.add(checkout, 3, 3);
            Button goToCheckout = new Button("Go to check out page");
            goToCheckout.setOnAction(e -> {
            	stage.setScene(cartScene);
            });
            gridMenu.add(goToCheckout, 3, 4);
            
            //Buttons
            HBox buttonsVB = new HBox(); 
            Button addOrder = new Button("Add item to cart");
            buttonsVB.getChildren().add(addOrder);
            buttonsVB.setAlignment(Pos.CENTER);
            gridMenu.add(buttonsVB, 2, 3);
            
            
            menuEvent = new MenuController("addProduct", itemsView, quantity);
            addOrder.setOnAction(menuEvent);
	    
      //-=-=-=-=-=-=-=-=-CHECKOUT CART-=-=-=-=-=-=-=-=-
            GridPane gridCart = new GridPane();
    		gridCart.setAlignment(Pos.CENTER);
    		gridCart.setHgap(5);
    		gridCart.setVgap(5);
    		gridCart.setPadding(new Insets(25, 25, 25, 25));

            Text cartTitle = new Text("Your cart");
            cartTitle.setFont(Font.font("Verdana", FontWeight.NORMAL, 20));
            gridCart.add(cartTitle, 0, 0, 2, 1);
            
            TextArea myCart = new TextArea();
            myCart.setPrefSize(250, 250);
            gridCart.add(myCart, 1, 1);
            //checkout
            
            menuEvent = new MenuController("addMenu", myCart, cartView, stage, cartScene);
            checkout.setOnAction(menuEvent);
            
            VBox confirmation = new VBox();
            confirmation.setSpacing(5);
            Button confirm = new Button("Confirm order");
            cartEvent = new CartController("addOrder");
            confirm.setOnAction(cartEvent);
            
            
            Button backToMenu = new Button("Back to menu");
            backToMenu.setOnAction(e -> {
            	stage.setScene(menuScene);
            });
            confirmation.getChildren().addAll(confirm, backToMenu);
            confirmation.setAlignment(Pos.CENTER);
            gridCart.add(confirmation, 1, 2);            
                       
      //-=-=-=-=-=-=-=-=-Scenes to stage-=-=-=-=-=-=-=-=-      
        loginScene = new Scene(gridLogin, 400, 400);
        registerScene = new Scene(gridRegister, 400, 400);
        menuScene = new Scene(gridMenu, 750, 750);
        cartScene = new Scene(gridCart, 600, 600);
        stage.setScene(loginScene);	        
        stage.setTitle("Welcome Back");
        stage.show();
        
	}
}
