package controller;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.sql.CallableStatement;
import java.sql.SQLException;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import server.DBService;

public class RegisterController implements EventHandler<ActionEvent>{

	private String action;
	private String id = "c1";
	private TextField username;
	private PasswordField password;
	private String salt;
	private TextField address;
	private TextField city;
	private TextField zip;
	private TextField phone;
	private TextField email;
	
	public RegisterController(String action, TextField username, PasswordField password, TextField address, TextField city, TextField zip, TextField phone, TextField email) {
		
		this.action = action;
		this.username = username;
		this.password = password;
		this.address = address;
		this.city = city;
		this.zip = zip;
		this.phone = phone;
		this.email = email;
	}
	
	public void register() {
		try {
			
			String query = "{call registerCustomer(?,?,?,?,?,?,?,?,?,?)}";
			CallableStatement statement = DBService.con.prepareCall(query);
			statement.setString(1, this.id);
			statement.setString(2, this.username.getText());
			statement.setBytes(3, this.hashPassword());
			statement.setString(4, this.salt);
			statement.setString(5, this.address.getText());
			statement.setString(6, this.city.getText());
			statement.setString(7, this.zip.getText());
			statement.setString(8, this.phone.getText());
			statement.setString(9, this.email.getText());
			statement.setString(10, "c3");
			statement.execute();
			if(statement != null) {
				
				statement.close();
			}
			System.out.println("Created Account");
			int number = Integer.parseInt(this.id.substring(1)) + 1;
			this.id = "c" + number;
		} catch (SQLException e) {
			
			e.printStackTrace();
			System.out.println("Cannot create account.");
		}
	}
	
	@Override
	public void handle(ActionEvent arg) {
		
		if(action.equals("register")) {
			
			this.generateSalt();
			this.register();
		}
	}
	
	public void generateSalt() {
		
		SecureRandom random = new SecureRandom();
		this.salt = new BigInteger(130, random).toString(32);
	}
	
	public byte[] hashPassword() {

		String hashAlgorithm = "PBEWithSHA1AndDESede";
		PBEKeySpec spec = new PBEKeySpec((this.password.getText() + this.salt).toCharArray());
		try {
			
			SecretKeyFactory skf = SecretKeyFactory.getInstance(hashAlgorithm);
			byte[] hash = skf.generateSecret(spec).getEncoded();
			return hash;
		} catch (NoSuchAlgorithmException e) {
			
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			
			e.printStackTrace();
		}
		return null;
	}
}
