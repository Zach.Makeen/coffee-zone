package controller;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.CallableStatement;
import java.sql.SQLException;
import java.sql.Types;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import server.DBService;

public class LoginController implements EventHandler<ActionEvent> {
	
	private TextField username;
	private PasswordField password;
	private String action;
	private String salt;
	
	public LoginController(String action, TextField username, PasswordField password) {
		
		this.action = action;
		this.username = username;
		this.password = password;
	}
	
    public void login() { 
    	
    	try {
    		
    		String query = "{call loginCustomer(?,?)}";
    		CallableStatement statement = DBService.con.prepareCall(query);
    		statement.setString(1, this.username.getText());
    		statement.setBytes(2, this.hashPassword());
    		statement.execute();
			if(statement != null) {
				
				statement.close();
			}
    	} catch (SQLException e) {
    		
    		e.printStackTrace();
    		System.out.println("Account does not exist");
    	}
    }
    
    private void getSalt() {
    	
    	try {
    		
    		String query2 = "{? = call getSalt( ? )}";
    		CallableStatement statement = DBService.con.prepareCall(query2);
    		statement.registerOutParameter(1, Types.VARCHAR);
    		statement.setString(2, username.getText());
    		statement.executeUpdate();
    		this.salt = statement.getString(1);
    		if(statement != null) {
				
				statement.close();
			}
    	} catch (SQLException e) {
    		
    		e.printStackTrace();
    		System.out.println("Something");
    	}
    }
    
    public byte[] hashPassword() {

		String hashAlgorithm = "PBEWithSHA1AndDESede";
		PBEKeySpec spec = new PBEKeySpec((this.password.getText() + this.salt).toCharArray());
		try {
			
			SecretKeyFactory skf = SecretKeyFactory.getInstance(hashAlgorithm);
			byte[] hash = skf.generateSecret(spec).getEncoded();
			return hash;
		} catch (NoSuchAlgorithmException e) {
			
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void handle(ActionEvent arg) {
		
		if(action.equals("login")) {
			
			this.getSalt();
			this.login();
		}
	}
	
	/**
	 * Step 1: Get hash + salt using username
	 * Step 2: Store hash and salt
	 * Step 3: Take the password that the customer login
	 */
}
