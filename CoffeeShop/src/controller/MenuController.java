package controller;

import java.sql.CallableStatement;
import java.sql.SQLException;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import server.DBService;

public class MenuController implements EventHandler<ActionEvent> {
	
	private String action;
	private String menuId = "m1";
	private String customerId = "c1";
	private String cartId = "s1";
	private ListView<String> name;
	private TextField quantity;
	private TextArea myCart;
	private TextArea cartView;
	private Stage stage;
	private Scene scene;
	private int count = 0;
	
	public MenuController(String action, ListView<String> name, TextField quantity) {
		
		this.action = action;
		this.name = name;
		this.quantity = quantity;
	}
	
	public MenuController(String action, TextArea myCart, TextArea cartView, Stage stage, Scene scene) {
		
		this.action = action;
		this.myCart = myCart;
		this.cartView = cartView;
		this.stage = stage;
		this.scene = scene;
	}
	
	/*public void addReferral() {
		
		try {
    		
    		String query = "{call addReferral(?,?)}";
    		CallableStatement statement = DBService.con.prepareCall(query);
    		statement.setString(1, "c1");
    		statement.execute();
			if(statement != null) {
				
				statement.close();
			}
    	} catch (SQLException e) {
    		
    		e.printStackTrace();
    		System.out.println("");
    	}
	}*/
	
	public void createMenu() {
		
		try {
    		
    		String query = "{call createMenu(?,?)}";
    		CallableStatement statement = DBService.con.prepareCall(query);
    		statement.setString(1, this.menuId);
    		statement.setString(2, this.customerId);
    		statement.execute();
			if(statement != null) {
				
				statement.close();
			}
    	} catch (SQLException e) {
    		
    		e.printStackTrace();
    		System.out.println("");
    	}
	}

	public void addProduct() {
	
		try {
			
			String query = "{call addProduct(?,?,?)}";
			CallableStatement statement = DBService.con.prepareCall(query);
			System.out.println(this.menuId);
			System.out.println(this.name.getSelectionModel().getSelectedItem().substring(0, this.name.getSelectionModel().getSelectedItem().length() - 6));
			System.out.println(Integer.parseInt(this.quantity.getText()));
			statement.setString(1, this.menuId);
			statement.setString(2, this.name.getSelectionModel().getSelectedItem().substring(0, this.name.getSelectionModel().getSelectedItem().length() - 6));
			statement.setInt(3, Integer.parseInt(this.quantity.getText()));
			statement.execute();
			if(statement != null) {
				
				statement.close();
			}
			myCart.setText(cartView.getText());
		} catch (SQLException e) {
		
			e.printStackTrace();
			System.out.println("Could not add item");
		}
	}
	
	public void addMenu() {
		
		try {
			
			System.out.println("Hello");
			String query = "{call addMenu(?,?,?)}";
			CallableStatement statement = DBService.con.prepareCall(query);
			statement.setString(1, this.cartId);
			statement.setString(2, this.menuId);
			statement.setInt(3, 1);
			statement.execute();
			if(statement != null) {
				
				statement.close();
			}
			int number = Integer.parseInt(this.menuId.substring(1)) + 1;
			this.menuId = "m" + number;
			stage.setScene(scene);
		} catch (SQLException e) {
		
		e.printStackTrace();
		System.out.println("Could not proceed to checkout");
		}
	}
	
	@Override
	public void handle(ActionEvent arg) {

		if(this.count == 0) {
			
			this.createMenu();
			this.count++;
		}

		System.out.println("Event Called");
		if(this.action.equals("addProduct")) {
			
			System.out.println("Called addProducts");
			this.addProduct();
		} else if(this.action.equals("addMenu")) {
			
			this.addMenu();
		}
	}
}
