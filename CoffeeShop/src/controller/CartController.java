package controller;

import java.sql.CallableStatement;
import java.sql.SQLException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import server.DBService;

public class CartController implements EventHandler<ActionEvent> {
	
	private String action;
	private String cartId = "s1";
	private String menuId = "m1";
	private String orderId = "o1";
	private int count = 0;

	public CartController(String action) {
	
		this.action = action;
	}
	
	public void createShoppingCart() {
		
		try {
			
			String query = "{call createShoppingCart(?)}";
			CallableStatement statement = DBService.con.prepareCall(query);
			statement.setString(1, this.cartId);
			statement.execute();
			if(statement != null) {
				
				statement.close();
			}
			int number = Integer.parseInt(this.menuId.substring(1)) + 1;
			this.cartId = "c" + number;
		} catch (SQLException e) {
		
		e.printStackTrace();
		System.out.println("");
		}
	}
	
	public void addOrder() {
		
		try {
			
			String query = "{call addOrder(?,?)}";
			CallableStatement statement = DBService.con.prepareCall(query);
			statement.setString(1, this.orderId);
			statement.setString(2, this.cartId);
			statement.execute();
			if(statement != null) {
				
				statement.close();
			}
			int number = Integer.parseInt(this.orderId.substring(1)) + 1;
			this.orderId = "o" + number;
		} catch (SQLException e) {
		
		e.printStackTrace();
		System.out.println("");
		}
	}
	
	@Override
	public void handle(ActionEvent arg) {
	
		if(this.count == 0) {
			
			this.createShoppingCart();
			this.count++;
		}
		
		if(action.equals("addOrder")) {
			
			this.addOrder();
		}
	}
}
