DROP TABLE cz_customers CASCADE CONSTRAINTS;
DROP TABLE cz_products CASCADE CONSTRAINTS;
DROP TABLE cz_menus CASCADE CONSTRAINTS;
DROP TABLE cz_menu_products CASCADE CONSTRAINTS;
DROP TABLE cz_shopping_carts CASCADE CONSTRAINTS;
DROP TABLE cz_cart_menus CASCADE CONSTRAINTS;
DROP TABLE cz_orders CASCADE CONSTRAINTS;

CREATE TABLE cz_customers (
    customer_id  VARCHAR2(5) PRIMARY KEY,
    username VARCHAR2(16),
    password VARCHAR(128),
    salt VARCHAR2(32),
    address VARCHAR2(16),
    city VARCHAR2(16),
    zip VARCHAR2(8),
    phone_number VARCHAR2(16),
    email VARCHAR2(16),
    referral_id  VARCHAR2(5),
    last_login DATE
);

CREATE TABLE cz_products (
    product_id VARCHAR2(5) PRIMARY KEY,
    name VARCHAR2(16),
    retail_price NUMBER(4, 2),
    in_stock NUMBER(10)
);

CREATE TABLE cz_menus (
    menu_id VARCHAR2(5) PRIMARY KEY,
    customer_id VARCHAR2(5),
    quantity  NUMBER(10),
    price NUMBER(4, 2),
    discount NUMBER(4, 2),
    FOREIGN KEY(customer_id) REFERENCES cz_customers(customer_id)
);

CREATE TABLE cz_menu_products (
    menu_id VARCHAR2(5),
    product_id VARCHAR2(5),
    quantity NUMBER(10),
    price NUMBER(4, 2),
    CONSTRAINT menu_products_pk PRIMARY KEY (menu_id, product_id),
    FOREIGN KEY(menu_id) REFERENCES cz_menus(menu_id),
    FOREIGN KEY(product_id) REFERENCES cz_products(product_id)
);

CREATE TABLE cz_shopping_carts (
    shopping_cart_id VARCHAR2(5) PRIMARY KEY,
    price NUMBER(4, 2)
);

CREATE TABLE cz_cart_menus (
    shopping_cart_id VARCHAR2(5),
    menu_id VARCHAR2(5),
    quantity NUMBER(10),
    price NUMBER(4, 2),
    CONSTRAINT cart_menus_pk PRIMARY KEY (shopping_cart_id, menu_id),
    FOREIGN KEY(shopping_cart_id) REFERENCES cz_shopping_carts(shopping_cart_id),
    FOREIGN KEY(menu_id) REFERENCES cz_menus(menu_id)
);

CREATE TABLE cz_orders (
    order_id  VARCHAR2(5) PRIMARY KEY,
    shopping_cart_id  VARCHAR2(5),
    order_date DATE,
    ship_date DATE,
    Ship_address VARCHAR2(16),
    Ship_city VARCHAR2(16),
    ship_zip VARCHAR2(16),
    ship_cost NUMBER(4, 2),
    FOREIGN KEY(shopping_cart_id) REFERENCES cz_shopping_carts(shopping_cart_id)
);

INSERT INTO cz_products VALUES('p1', 'americano', '3.00', '60');
INSERT INTO cz_products VALUES('p2', 'cappucino', '3.50', '60');
INSERT INTO cz_products VALUES('p3', 'latte', '3.00', '60');
INSERT INTO cz_products VALUES('p4', 'expresso', '2.00', '60');
INSERT INTO cz_products VALUES('p5', 'croissant', '2.00', '60');
INSERT INTO cz_products VALUES('p6', 'english muffin', '4.00', '60');
INSERT INTO cz_products VALUES('p7', 'bagel and cream', '2.50', '60');
INSERT INTO cz_products VALUES('p8', 'cinnamon roll', '3.00', '60');
INSERT INTO cz_products VALUES('p9', 'sugar', '0.15', '60');
INSERT INTO cz_products VALUES('p10', 'milk', '0.15', '60');