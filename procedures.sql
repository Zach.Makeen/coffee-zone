CREATE OR REPLACE
    PROCEDURE registerCustomer (
        theCustomerId cz_customers.customer_id%TYPE,
        theUsername cz_customers.username%TYPE,
        thePassword cz_customers.password%TYPE,
        theSalt cz_customers.salt%TYPE,
        theAddress cz_customers.address%TYPE,
        theCity cz_customers.city%TYPE,
        theZip cz_customers.zip%TYPE,
        thePhoneNumber cz_customers.phone_number%TYPE,
        theEmail cz_customers.email%TYPE,
        theReferralId cz_customers.referral_id%TYPE
    ) AS
        theLastLogin cz_customers.last_login%TYPE;
        /*duplicate_value EXCEPTION;
        PRAGMA EXCEPTION_INIT(duplicate_value, -20000);*/
    BEGIN
        SELECT CURRENT_TIMESTAMP INTO theLastLogin
        FROM dual;
        INSERT INTO cz_customers VALUES (theCustomerId, theUsername, thePassword, theSalt, theAddress, theCity, theZip, thePhoneNumber, theEmail, theReferralId, theLastLogin);
    /*EXCEPTION
        WHEN duplicate_value THEN
            DBMS_OUTPUT.PUT_LINE('registerCustomer: A customer with id ' || theCustomerId || ' already exists.');
            RAISE_APPLICATION_ERROR(-20000, 'Username alright exists');*/
    END;
    
EXECUTE registerCustomer('c0001', 'zacharie', 'password', 'salt', 'address', 'city', 'zip', 'phone number', 'email', 'c0003');
DELETE FROM cz_customers WHERE customer_id = 'c1';

SELECT CURRENT_TIMESTAMP
FROM dual;

CREATE OR REPLACE
    FUNCTION getSalt (
        theUsername cz_customers.username%TYPE
    ) RETURN VARCHAR2 AS
        theSalt cz_customers.salt%TYPE;
        no_username EXCEPTION;
        PRAGMA EXCEPTION_INIT(no_username, -20001);
    BEGIN
        SELECT salt INTO theSalt
        FROM cz_customers
        WHERE username = theUsername;
        RETURN theSalt;
    EXCEPTION
        WHEN no_data_found THEN
            DBMS_OUTPUT.PUT_LINE('The username or password is incorrect.');
            RAISE_APPLICATION_ERROR(-20001, 'The username does not exists');
    END;

CREATE OR REPLACE
    PROCEDURE loginCustomer (
        theUsername cz_customers.username%TYPE,
        thePassword cz_customers.password%TYPE
    ) AS
        theCustomerId cz_customers.customer_id%TYPE;
        theLastLogin  cz_customers.last_login%TYPE;
        missing_login_info EXCEPTION;
        PRAGMA EXCEPTION_INIT(missing_login_info, -20002);
    BEGIN
        SELECT customer_id, CURRENT_TIMESTAMP INTO theCustomerId, theLastLogin
        FROM cz_customers
        WHERE username = theUsername AND password = thePassword;
        
        UPDATE cz_customers SET last_login = theLastLogin WHERE customer_id = theCustomerId;
    EXCEPTION
        WHEN no_data_found THEN
            DBMS_OUTPUT.PUT_LINE('The username or password is incorrect.');
            RAISE_APPLICATION_ERROR(-20002, 'The username or password is incorrect.');
    END;

EXECUTE loginCustomer('zacharie', 'password');

CREATE OR REPLACE
    PROCEDURE addReferral (
        theCustomerId cz_customers.customer_id%TYPE,
        theReferralId cz_customers.referral_id%TYPE
    ) AS
    BEGIN
        UPDATE cz_customers SET referral_id = theReferralId WHERE customer_id = theCustomerId;
        UPDATE cz_menus SET discount = discount + 5 WHERE customer_id = theReferralId;
    END;

CREATE OR REPLACE
    PROCEDURE createMenu (
        theMenuId cz_menus.menu_id%TYPE,
        theCustomerId cz_menus.customer_id%TYPE
    ) AS
    BEGIN
        INSERT INTO cz_menus VALUES (theMenuId, theCustomerId, '1', '0', '0');
    END;
    
EXECUTE createMenu('m0001', 'c0001');
DELETE FROM cz_menus WHERE menu_id = 'm0001';

CREATE OR REPLACE
    PROCEDURE addProduct (
        theMenuId cz_menus.menu_id%TYPE,
        theName cz_products.name%TYPE,
        theQuantity cz_menu_products.quantity%TYPE
    ) AS
        theProductId cz_products.product_id%TYPE;
        thePrice cz_menu_products.price%TYPE;
    BEGIN
        SELECT product_id, (retail_price * theQuantity) INTO theProductId, thePrice
        FROM cz_products
        WHERE name = theName;        
        
        INSERT INTO cz_menu_products VALUES (theMenuId, theProductId, theQuantity, thePrice);
    END;
    
EXECUTE addProduct('m1', 'americano', 3);
DELETE FROM cz_menu_products WHERE menu_id = 'm1';

CREATE OR REPLACE TRIGGER after_addProduct_call
    BEFORE INSERT
    ON cz_menu_products
    FOR EACH ROW
    DECLARE
        theInStock cz_products.in_stock%TYPE;
        theName cz_products.name%TYPE;
        thePrice cz_menus.price%TYPE;
        stock_empty EXCEPTION;
        PRAGMA EXCEPTION_INIT(stock_empty, -20003);
    BEGIN
        SELECT in_stock, name INTO theInStock, theName
        FROM cz_products
        WHERE product_id = :NEW.product_id;
        theInStock := theInStock - :NEW.quantity;
        UPDATE cz_products SET in_stock = theInStock WHERE product_id = :NEW.product_id;
        IF(theInStock < 10) THEN
            RAISE_APPLICATION_ERROR(-20003, theName || ' is no longer in stock.');
        END IF;
        
        SELECT price INTO thePrice
        FROM cz_menus
        WHERE menu_id = :NEW.menu_id;
        thePrice := thePrice + :NEW.price;
        UPDATE cz_menus SET price = thePrice WHERE menu_id = :NEW.menu_id;
    END;

CREATE OR REPLACE
    PROCEDURE createShoppingCart (
        theShoppingCartId cz_shopping_carts.shopping_cart_id%TYPE
    ) AS
    BEGIN
        INSERT INTO cz_shopping_carts VALUES (theShoppingCartId, '0');
    END;

EXECUTE createShoppingCart('s1');
DELETE FROM cz_ShoppingCart WHERE shopping_cart_id = 's0001';

CREATE OR REPLACE
    PROCEDURE addMenu (
        theShoppingCartId cz_shopping_carts.shopping_cart_id%TYPE,
        theMenuId cz_menus.menu_id%TYPE,
        theQuantity cz_cart_menus.quantity%TYPE
    ) AS
        thePrice cz_cart_menus.price%TYPE;
    BEGIN
        SELECT (price * theQuantity) INTO thePrice
        FROM cz_menus
        WHERE menu_id = theMenuId;        
        
        INSERT INTO cz_cart_menus VALUES (theShoppingCartId, theMenuId, theQuantity, thePrice);
    END;
    
EXECUTE addMenu('s0001', 'm0001', 2);
DELETE FROM cz_cart_menus WHERE shopping_cart_id = 's1';

CREATE OR REPLACE TRIGGER after_addMenu_call
    BEFORE INSERT
    ON cz_cart_menus
    FOR EACH ROW
    DECLARE
        thePrice cz_shopping_carts.price%TYPE;
    BEGIN
        SELECT price INTO thePrice
        FROM cz_shopping_carts
        WHERE shopping_cart_id = :NEW.shopping_cart_id;
        thePrice := thePrice + :NEW.price;
        UPDATE cz_shopping_carts SET price = thePrice WHERE shopping_cart_id = :NEW.shopping_cart_id;
    END;

CREATE OR REPLACE
    PROCEDURE addOrder (
        theOrderId cz_orders.order_id%TYPE,
        theShoppingCartId cz_orders.shopping_cart_id%TYPE
    )AS
        theOrderDate cz_orders.order_date%TYPE;
        theShipDate cz_orders.ship_date%TYPE;
        theShipAddress cz_orders.ship_address%TYPE;
        theShipCity cz_orders.ship_city%TYPE;
        theShipZip cz_orders.ship_zip%TYPE;
        theShipCost cz_orders.ship_cost%TYPE;
    BEGIN  
        SELECT CURRENT_TIMESTAMP, (CURRENT_TIMESTAMP + 5), address, city, zip INTO theOrderDate, theShipDate, theShipAddress, theShipCity, theShipZip
        FROM cz_customers
        JOIN cz_menus USING(customer_id)
        JOIN cz_cart_menus USING(menu_id)
        JOIN cz_shopping_carts USING(shopping_cart_id)
        WHERE shopping_cart_id = theShoppingCartId;
        
        SELECT price INTO theShipCost
        FROM cz_shopping_carts
        WHERE shopping_cart_id = theShoppingCartId;
        
        INSERT INTO cz_orders VALUES(theOrderId, theShoppingCartId, theOrderDate, theShipDate, theShipAddress, theShipCity, theShipZip, theShipCost);
    END;

EXECUTE addORder('o0001', 's0001');